package com.mobilus.contagio.util;

import java.util.UUID;

/**
 * Created by user on 07/07/2015.
 */
public class TextUtil {
    public static String generateRandomString() {
        String randString = UUID.randomUUID().toString();
        randString = randString.replace("-", "");
        String currentTimeMillis = System.currentTimeMillis() + "";
        try {
            randString = randString.substring(currentTimeMillis.length());
            randString = currentTimeMillis + "-" + randString;
        } catch (IndexOutOfBoundsException e) {
            randString = currentTimeMillis;
        }

        return randString;
    }
}
