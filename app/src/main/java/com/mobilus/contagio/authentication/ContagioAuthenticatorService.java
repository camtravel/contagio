package com.mobilus.contagio.authentication;

import android.app.Service;
import android.content.Intent;
import android.os.IBinder;

/**
 * Created by root on 10/16/15.
 */
public class ContagioAuthenticatorService extends Service {
    @Override
    public IBinder onBind(Intent intent) {

        ContagioAuthenticator authenticator = new ContagioAuthenticator(this);
        return authenticator.getIBinder();
    }
}
