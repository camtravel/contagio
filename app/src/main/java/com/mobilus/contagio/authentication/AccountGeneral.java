package com.mobilus.contagio.authentication;

import android.accounts.Account;
import android.accounts.AccountManager;
import android.accounts.AccountManagerFuture;
import android.app.Activity;
import android.os.Bundle;
import android.util.Log;

import com.mobilus.contagio.config.Credential;
import com.mobilus.contagio.model.User;

/**
 * Created by root on 10/16/15.
 */
public class AccountGeneral {
    private static final String TAG = "AccountGeneral";
    /**
     * Account type id
     */
    public static final String ACCOUNT_TYPE = "com.mobilus.contagio";

    /**
     * Account name
     */
    public static final String ACCOUNT_NAME = "Contagio";

    /**
     * Auth token types
     */
    public static final String AUTHTOKEN_TYPE_READ_ONLY = "Read only";
    public static final String AUTHTOKEN_TYPE_READ_ONLY_LABEL = "Read only access to a Contagio account";

    public static final String AUTHTOKEN_TYPE_FULL_ACCESS = "Full access";
    public static final String AUTHTOKEN_TYPE_FULL_ACCESS_LABEL = "Full access to a Contagio account";

    public static final ServerAuthenticate sServerAuthenticate = new ContagioServerAuthenticate();

    public static boolean initLoggedInUser(Activity activity) {
        Credential.USER = User.findById(User.class, 1L);

        return true;
    }

    public static boolean initLoggedInUser() {
        return initLoggedInUser(null);
    }

    public static void initExistingAuthToken(Activity activity) {
        Account[] accounts = AccountManager.get(activity.getBaseContext()).getAccountsByType(AccountGeneral.ACCOUNT_TYPE);

        if (accounts.length > 0) {
            Account account = accounts[0];
            final AccountManagerFuture<Bundle> future = AccountManager.get(activity.getBaseContext()).getAuthToken(account, AccountGeneral.AUTHTOKEN_TYPE_FULL_ACCESS, null, activity, null, null);


            new Thread(new Runnable() {
                @Override
                public void run() {
                    try {
                        Bundle bnd = future.getResult();

//                        Credential.AUTH_TOKEN = bnd.getString(AccountManager.KEY_AUTHTOKEN);
                        String authToken = bnd.getString(AccountManager.KEY_AUTHTOKEN);
                        User user = Credential.getUser();
                        if(user != null){
                            user.setAccessToken(authToken);
                            user.save();
                        }

                        Log.d(TAG, "GetToken Bundle is " + bnd);
                    } catch (Exception e) {
                        e.printStackTrace();
                    }
                }
            }).start();
        }
    }

    public static void removeAccount(Activity activity) {
        Account[] accounts = AccountManager.get(activity.getApplicationContext()).getAccountsByType(AccountGeneral.ACCOUNT_TYPE);

        for (int index = 0; index < accounts.length; index++) {
            AccountManager.get(activity.getApplicationContext()).removeAccount(accounts[index], null, null);
        }
    }
}
