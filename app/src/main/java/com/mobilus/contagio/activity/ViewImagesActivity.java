package com.mobilus.contagio.activity;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.support.v4.view.PagerAdapter;
import android.support.v4.view.ViewPager;
import android.support.v4.view.ViewPager.LayoutParams;
import android.view.View;
import android.view.ViewGroup;

import com.koushikdutta.ion.Ion;
import com.mobilus.contagio.R;
import com.mobilus.contagio.config.Appconfig;
import com.mobilus.contagio.ui.HackyViewPager;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import uk.co.senab.photoview.PhotoView;

/**
 * Created by user on 21/10/2015.
 */
public class ViewImagesActivity extends Activity {
    private ViewPager mViewPager;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        setContentView(R.layout.report_view_images);
        mViewPager = (HackyViewPager) findViewById(R.id.view_pager);
        setContentView(mViewPager);

        Intent intent = getIntent();
        String images = intent.getStringExtra("images");
        Integer position = intent.getIntExtra("position", -1);
        Integer itemType = intent.getIntExtra("itemType", Appconfig.NEWSFEED_REPORT);

        mViewPager.setAdapter(new ReportImagesPagerAdapter(images, itemType));
        mViewPager.setCurrentItem(position);
    }

    static class ReportImagesPagerAdapter extends PagerAdapter {
        private static final String TAG = "ImagesPagerAdapter";
        private JSONArray sDrawables;
        private int itemType;

        public ReportImagesPagerAdapter(String imgs, int itemType) {
            try {
                sDrawables = new JSONArray(imgs);
                this.itemType = itemType;
            } catch (JSONException e) {
                e.printStackTrace();
            }
        }
        @Override
        public int getCount() {
            return sDrawables.length();
        }

        @Override
        public View instantiateItem(ViewGroup container, int position) {
            PhotoView photoView = new PhotoView(container.getContext());
            JSONObject img;
            try {
                img = sDrawables.getJSONObject(position);
                String imgUrl = "";

                if(itemType == Appconfig.NEWSFEED_REPORT || itemType == Appconfig.NEWSFEED_LOCATION) {
                    imgUrl = Appconfig.REPORT_IMAGE_ASSET_URL + img.optString("name");
                }else if (itemType == Appconfig.NEWSFEED_EVENT){
                    imgUrl = Appconfig.EVENT_IMAGE_ASSET_URL + img.optString("name");
                }

                if (img.optString("path").length() > 0) {
                    imgUrl = img.optString("path");
                }

                if(imgUrl != ""){
                    Ion.with(photoView).load(imgUrl);
                }
            } catch (JSONException e) {
                e.printStackTrace();
            }

            // Now just add PhotoView to ViewPager and return it
            container.addView(photoView, LayoutParams.MATCH_PARENT, LayoutParams.MATCH_PARENT);

            return photoView;
        }

        @Override
        public void destroyItem(ViewGroup container, int position, Object object) {
            container.removeView((View) object);
        }

        @Override
        public boolean isViewFromObject(View view, Object object) {
            return view == object;
        }

    }
}
