package com.mobilus.contagio.model;

import android.text.TextUtils;

import com.orm.SugarRecord;

import java.util.List;

/**
 * Created by Fikran on 29 Okt 2015.
 */
public class Preference extends SugarRecord {
    public static final String KEY__IS_LOGIN_SKIPPED = "key_is_login_skipped";

    String key = "";
    String value = "";

    public Preference(){}

    private static Preference findByKey(String key){
        if(TextUtils.isEmpty(key)){
            return null;
        }

        List<Preference> preferences = find(Preference.class, "KEY LIKE ?", new String[]{key}, null, null, "1");

        return !preferences.isEmpty() ? preferences.get(0) : null;
    }

    public static void set(String key, String value){
        if(TextUtils.isEmpty(key)){
            return;
        }

        Preference preference = findByKey(key);
        if(preference == null){
            preference = new Preference();
            preference.key = key;
        }
        preference.value = value;

        preference.save();
    }

    public static void set(String key, boolean value){
        set(key, "" + value);
    }

    public static String get(String key){
        Preference preference = findByKey(key);
        if(preference != null){
            return preference.value;
        }

        return "";
    }

    public static boolean getAsBoolean(String key){
        String value = get(key);

        return Boolean.parseBoolean(value);
    }
}
