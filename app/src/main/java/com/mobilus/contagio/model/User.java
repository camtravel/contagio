package com.mobilus.contagio.model;

import com.orm.SugarRecord;

/**
 * Created by Fikran on 13 Okt 2015.
 */
public class User extends SugarRecord {
    public static int GENDER_MALE = 1;
    public static int GENDER_FEMALE = 2;

    private int idServer = 0;
    private String accessToken = "";
    private String email = "";
    private String password = "";
    private String name = "";
    private String phone = "";
    private int gender = GENDER_MALE;
    private String birthDay = "";
    private String fbId = "";

    public int getIdServer() {
        return idServer;
    }

    public void setIdServer(int idServer) {
        this.idServer = idServer;
    }

    public String getAccessToken() {
        return accessToken;
    }

    public void setAccessToken(String accessToken) {
        this.accessToken = accessToken;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getPhone() {
        return phone;
    }

    public void setPhone(String phone) {
        this.phone = phone;
    }

    public int getGender() {
        return gender;
    }

    public void setGender(int gender) {
        this.gender = gender;
    }

    public String getBirthDay() {
        return birthDay;
    }

    public void setBirthDay(String birthDay) {
        this.birthDay = birthDay;
    }

    public String getFbId() {
        return fbId;
    }

    public void setFbId(String fbId) {
        this.fbId = fbId;
    }
}
